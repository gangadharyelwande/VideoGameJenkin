/**
 * 
 */
package videoGame.videoGame;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author gyelwand
 *
 */
class PerformAuditions {

	private List<Performers> allPerformersList = new ArrayList<Performers>();
	
	
	public int addPerformers(int unionID) throws IllegalArgumentException{
		
		if(unionID<0) {
			throw new IllegalArgumentException("Union Id should not be negative");
		}
		
		allPerformersList.add(new Performers(unionID));
		return 1;
	}


	public int beginAuditions() {
		for(int i=0; i<allPerformersList.size(); i++) {
			allPerformersList.get(i).performAudition();
	}
		return 1;
  }

	public int addVocalists(int unionID, char vocalType) {
		allPerformersList.add(new Vocalists(unionID, vocalType));
		return 1;
	}


	public int addDancers(int unionID, String keyType) {
		allPerformersList.add(new Dancers(unionID, keyType));
		return 1;
		
	}
	
}	
