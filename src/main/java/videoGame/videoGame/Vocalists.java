/**
 * 
 */
package videoGame.videoGame;

import org.apache.log4j.Logger;

/**
 * @author gyelwand
 *
 */
class Vocalists extends Performers{

	private int unionID;
	private char vocalType;
	private int volume;
	
	final static Logger logger = Logger.getLogger(Vocalists.class);
	
	/**
	 * @return the volume
	 */
	public int getVolume() {
		return volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(int volume) {
		this.volume = volume;
	}

	/**
	 * @return the vocalType
	 */
	public char getVocalType() {
		return vocalType;
	}

	/**
	 * @param vocalType the vocalType to set
	 */
	public void setVocalType(char vocalType) {
		this.vocalType = vocalType;
	}

	public Vocalists(int unionID, char vocalType) {
		super(unionID);
		this.vocalType=vocalType;
	}

	/**
	 * @return the unionID
	 */
	public int getUnionID() {
		return unionID;
	}

	/**
	 * @param unionID the unionID to set
	 */
	public void setUnionID(int unionID) {
		this.unionID = unionID;
	}

	
	public Vocalists(int performerID) {
		super(performerID);
	}

	public Vocalists(int unionID, char vocalType, int volume) throws IllegalArgumentException{
		
		
		super(unionID);
		
		if(volume<0) {
			throw new IllegalArgumentException("Negative volume not allowed");
		}
		
		if(volume>10) {
			throw new IllegalArgumentException("Volume should not be greater than 10");
		}
		this.vocalType=vocalType;
		this.volume=volume;	
	}
	
	@Override
	int performAudition() {
		if(this.volume<=0) 
			logger.info(this.unionID+" "+this.vocalType);
		else 
			logger.info("I sing in the key of -"+this.vocalType+ "-"+this.volume);
		
		return 1;
		
	}
}
