/**
 * 
 */
package videoGame.videoGame;

import org.apache.log4j.Logger;

/**
 * @author gyelwand
 *
 */
class Performers {
	final static Logger logger = Logger.getLogger(Performers.class);
	
	private final int unionId;

	Performers(int performerID) throws IllegalArgumentException{
		
		if(performerID<0) {
			throw new IllegalArgumentException("negative union ID not allowed");
		}
		this.unionId=performerID;
	}

	int performAudition() {
		logger.info(this.unionId +" -performer");
		return 1;
	}

}
