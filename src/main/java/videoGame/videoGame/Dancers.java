package videoGame.videoGame;

import org.apache.log4j.Logger;

class Dancers extends Performers{

	final static Logger logger = Logger.getLogger(Dancers.class);
	
	private int unionID;
	private String keyType;
	
	public Dancers(int performerID) {
		super(performerID);
		
	}

	public Dancers(int unionID, String keyType) {
		super(unionID);
		this.keyType=keyType;
	}
	
	@Override
	int performAudition() {
		logger.info(this.keyType+"-"+this.unionID+"-dancer");
		return 1;
	}

}
