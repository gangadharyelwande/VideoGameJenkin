/**
 * 
 */
package videoGame.videoGame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import junit.framework.TestCase;

/**
 * @author gyelwand
 *
 */
public class PerformAuditionsTest{
	
	PerformAuditions performAuditions;
	
	@Before
	public void initializeData() {
		performAuditions= new PerformAuditions();
		performAuditions.addPerformers(101);
		performAuditions.addPerformers(102);
		performAuditions.addPerformers(103);
		performAuditions.addPerformers(104);
		
		performAuditions.addVocalists(105, 'G');
		
		performAuditions.addDancers(106, "hiphop");
		performAuditions.addDancers(107, "tap");
	}
	
	@Test
	public void auditionWithPositiveUniqueIdTest() {
		 assertEquals(1,performAuditions.beginAuditions());
	}
	
	@Test
	public void addVocalistWithPositiveUniqueIdTest() {
		 assertEquals(1,performAuditions.addVocalists(108, 'G'));
		 
	}
	
	@Test
	public void addDancerWithPositiveUniqueIdTest() {
		 assertEquals(1,performAuditions.addDancers(109, "Jazz"));
		 
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void negativeUnionIdTest() {
		exception.expect(IllegalArgumentException.class);
	    exception.expectMessage("Union Id should not be negative");
	    performAuditions= new PerformAuditions();
		performAuditions.addPerformers(-101);
	}
	
	

}
