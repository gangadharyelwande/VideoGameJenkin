package videoGame.videoGame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class PerformersTest{

	Performers performers;
	@Before
	public void initializeData() {
		performers = new Performers(1);
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	
	@Test
	public void unionIDwithPositiveValueTest() {
		 assertEquals(1,performers.performAudition());
	}

	@Test
	public void negativeUnionIdTest() {
		exception.expect(IllegalArgumentException.class);
	    exception.expectMessage("negative union ID not allowed");
		performers= new Performers(-40);

	}
	 
	
}
