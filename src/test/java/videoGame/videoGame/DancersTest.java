package videoGame.videoGame;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class DancersTest{
	
	Performers performAuditions;	
	
	@Before
	public void initializeData() {
		performAuditions=new Dancers(106, "hiphop");
	}
	
	@Test
	public void addDancerWithPositiveUniqueIdTest() {
		 assertEquals(1,performAuditions.performAudition());
		 
	}
}
