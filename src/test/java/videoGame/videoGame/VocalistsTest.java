package videoGame.videoGame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistsTest{
	
	Vocalists vocalistsObject;
	
	Performers performAuditions1;
	Performers performAuditions2;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();

	
	@Before
	public void initializeData() {
		performAuditions1= 	new Vocalists(1, 'G');
		performAuditions2= 	new Vocalists(1, 'G',10);
	}
	
	@Test
	public void auditionWithoutVolumeTest() {
		 assertEquals(1,performAuditions1.performAudition());
	}
	
	@Test
	public void auditionWithVolumeTest() {
		 assertEquals(1,performAuditions2.performAudition());
	}
	
	@Test
	public void negativeVolumeTest() {
		exception.expect(IllegalArgumentException.class);
	    exception.expectMessage("Negative volume not allowed");
	    vocalistsObject= new Vocalists(1, 'G',-2);
	}
	
	@Test
	public void maximumVolumeTest() {
		exception.expect(IllegalArgumentException.class);
	    exception.expectMessage("Volume should not be greater than 10");
	    vocalistsObject= new Vocalists(1, 'G', 25);
	}
	
}
